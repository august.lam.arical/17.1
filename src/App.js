import * as React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { withStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import data24 from "./24hrs.json";
import dataWeek from "./week.json";
import dataMonths from "./12m.json";
import {
  Chart,
  BarSeries,
  ArgumentAxis,
  ValueAxis
} from '@devexpress/dx-react-chart-material-ui';
import { ValueScale, EventTracker } from '@devexpress/dx-react-chart';
import { scaleLinear } from 'd3-scale';

const theme = createMuiTheme({
  typography: {
    fontFamily: 'inter',
    fontSize: 12,
    lineHeight: "15px",
    fontStyle: 'normal',
    alignItems: 'center',
    display: 'flex',
    fontWeightMedium: 400,
    fontWeightBold: 700,
    h1: {//heading2
      fontSize: 32,
      lineHeight: "39px",
      fontWeight: 'bold',
    },
    h2: {//heading3
      fontSize: 26,
      lineHeight: "31px",
      fontWeight: 'bold',
    },
    h3: {//heading4
      fontSize: 22,
      lineHeight: "27px",
      fontWeight: 'bold',
    },
    h4: {//heading5
      fontSize: 18,
      lineHeight: "22px",
      fontWeight: 'bold',
    },
    h5: {//heading6
      fontSize: 16,
      lineHeight: "19px",
      fontWeight: 'bold',
    },
    h6: {//heading7
      fontSize: 14,
      lineHeight: "17px",
      fontWeight: 'bold',
    },
    subtitle1: {//p2
      fontSize: 16,
      lineHeight: "19px",
      fontWeight: 'normal',
    },
    subtitle2: {//p4
      fontSize: 12,
      lineHeight: "15px",
      fontWeight: 'normal',
    },
    button: {
      textTransform: "none"
    }
  },
  palette: {
    primary: {
      main: "#FFFFFF",
    },
    secondary: {
      main: "#0000EE",
    },
  },
  overrides: {
    MuiButton: {
      containedPrimary: {
        borderRadius: '8px',
        border: '1px solid #828282',
        boxSizing: 'border-box',
        height: '28px',
      },
      containedSecondary: {
        borderRadius: '8px',
        boxSizing: 'border-box',
        height: '28px',
      }
    }
  },
});

const style = theme => ({
  count: {
    fontSize: 26,
    lineHeight: "31px",
    fontWeight: 'bold',
    color: "#0000EE",
  },
  button: {
    marginBottom: '12px',
  }
});

var scale = () => scaleLinear();
var modifyDomain = domain => [domain[0], 1000, 5000];

const compare = (
  { series, point }, { series: targetSeries, point: targetPoint },
) => series === targetSeries && point === targetPoint;

class App extends React.PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      hover: null,
      data24,
      dataWeek,
      dataMonths,
      selection24: [],
      selectionWeek: [],
      selectionMonths: [],
      period: true,
    };

    this.click = ({ targets }) => {
      // console.log(targets);
      const target = targets[0];
      if (target) {
        switch (target.series) {
          case "last24hoursTable":
            this.setState(({ selection24 }) => ({
              selection24: selection24[0] && compare(selection24[0], target) ? [] : [target],
            }));

            break;
          case "weekTable":
            this.setState(({ selectionWeek }) => ({
              selectionWeek: selectionWeek[0] && compare(selectionWeek[0], target) ? [] : [target],
            }));
            break;
          case "last12monthsTable":
            this.setState(({ selectionMonths }) => ({
              selectionMonths: selectionMonths[0] && compare(selectionMonths[0], target) ? [] : [target],
            }));
            break;
          default:
            return;
        }
      }
    };
    this.changeHover = ({ targets }) => {
      const target = targets[0];
      // console.log(target);
      if (target) 
        this.setState(({ hover }) => ({ hover: target }))
      else
        this.setState(({ hover }) => ({ hover: null }))
    }

  }


  render() {
    var dsum = 0; var wsum = 0; var msum = 0;
    const { classes } = this.props;
    const {
      selection24,
      selectionWeek,
      selectionMonths, } = this.state;


    const load = () => {
      const m = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
      const days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

      for (var i in data24) {
        var n = new Date(data24[i]['date']);
        dsum += data24[i]['no'];
        if (n.getUTCHours() === 0)
          data24[i]['hr'] = "24";
        else if (n.getUTCHours() < 10)
          data24[i]['hr'] = '0' + n.getUTCHours().toString();
        else
          data24[i]['hr'] = n.getUTCHours().toString();
      }
      // eslint-disable-next-line
      for (var i in dataWeek) {
        dataWeek[i]['wk'] = days[i];
        wsum += dataWeek[i]['no'];
      }
      var y = new Date().getUTCFullYear();
      // eslint-disable-next-line
      for (var i in dataMonths) {
        // eslint-disable-next-line
        var n = new Date(dataMonths[i]['date']);
        dataMonths[i]['month'] = m[n.getUTCMonth()];
        if (i === 0 || n.getUTCFullYear() !== y) {
          y = n.getUTCFullYear();
          dataMonths[i]['month'] += ("\n" + n.getUTCFullYear().toString());
        }
        msum += dataMonths[i]['no'];
      }
    }

    const handleTable = (event) => {
      // console.log(event);
      if (event.target.textContent === "12 months" && this.state.period === false)
        this.setState({ period: true, selectionMonths: [] });
      else if (event.target.textContent === "1 Week" && this.state.period === true)
        this.setState({ period: false, selectionWeek: [] });
      // console.log(this.period);
    }

    function getTime(props) {
      // console.log(props);
      if (props.text === "24" || props.text === data24[0]['hr'])
        return 'AM';
      else if (props.text === "12")
        return 'PM';
      return '';
    }
    const getPath = (x, width, y, y1) => `M ${x} ${y1}
    h ${-width}
    a ${3} ${3} ${0} ${0} ${1} ${-3} ${-3}
    v ${-y}
    a ${3} ${3} ${0} ${0} ${1} ${3} ${-3}
    h ${width}
    a ${3} ${3} ${0} ${0} ${1} ${3} ${3}
    v ${y}
    a ${3} ${3} ${0} ${0} ${1} ${-3} ${3}
    Z`;

    function NewlineText(props) {
      // console.log(props);
      return (
        <React.Fragment >
          <ArgumentAxis.Label {...props} text={!(props.text.charAt(0) >= '0' && props.text.charAt(0) <= '9') ? props.text.split("\n")[0] : props.text} ></ArgumentAxis.Label>
          <ArgumentAxis.Label {...props} text={(props.text.charAt(0) >= '0' && props.text.charAt(0) <= '9') ? getTime(props) : props.text.split("\n")[1]} y={'24'} />
        </React.Fragment>
      );
    }
    const newBar1 = ({
      arg, barWidth, maxBarWidth, val, startVal, color, value, style, index,
    }) => {
      const width = maxBarWidth * barWidth;
      // console.log(index);
      return (
        <React.Fragment>
          <path
            d={getPath(arg + width / 2, width, startVal - val - 6, startVal)} fill={selection24.length !== 0 && selection24[0].point === index ? "#909090" :
              this.state.hover!==null&& this.state.hover.point===index&&this.state.hover.series==="last24hoursTable"?"#42A5F5" : color} />
        </React.Fragment>
      );
    };
    const newBar2 = ({
      arg, barWidth, maxBarWidth, val, startVal, color, value, style, index,
    }) => {
      const width = maxBarWidth * barWidth;
      // console.log(index);
      return (
        <React.Fragment>
          <path d={getPath(arg + width / 2, width, startVal - val - 6, startVal)} fill={selectionWeek.length !== 0 && selectionWeek[0].point === index ? "#909090" :
            this.state.hover!==null&& this.state.hover.point===index&&this.state.hover.series==="weekTable"? "#42A5F5" : color} />
        </React.Fragment>
      );
    };
    const newBar3 = ({
      arg, barWidth, maxBarWidth, val, startVal, color, value, style, index,
    }) => {
      const width = maxBarWidth * barWidth;
      // console.log(index);
      return (
        <React.Fragment>
          <path d={getPath(arg + width / 2, width, startVal - val - 6, startVal)} fill={selectionMonths.length !== 0 && selectionMonths[0].point === index ? "#909090" : 
          this.state.hover!==null&& this.state.hover.point===index&&this.state.hover.series==="last12monthsTable"? "#42A5F5" : color} />
        </React.Fragment>
      );
    };
    const last24hoursTable = () => {
      // console.log(selection24)
      return (
        <div>
          <Chart
            data={data24}
            height={159}
            >
            <ValueScale factory={scale} modifyDomain={modifyDomain} />
            <ArgumentAxis labelComponent={NewlineText} />
            <ValueAxis max={24} />

            <BarSeries
              name="last24hoursTable"
              valueField="no"
              argumentField="hr"
              barWidth={0.4165}
              color={'#0000EE'}
              pointComponent={newBar1}
            />
            <EventTracker onClick={this.click}  onPointerMove={this.changeHover}/>
            {/* <HoverState hover={hover} onHoverChange={console.log("this.changeHover")} /> */}
            {/* <SelectionState selection={selection24} /> */}
          </Chart>
        </div>
      );
    }

    const weekTable = () => {
      return (
        <Chart
          data={dataWeek}
          height={145}
        >
          <ValueScale factory={scale} modifyDomain={modifyDomain} />
          <ArgumentAxis />
          <ValueAxis max={24} />
          <BarSeries
            name="weekTable"
            valueField="no"
            argumentField="wk"
            barWidth={0.2916}
            color={'#0000EE'}
            pointComponent={newBar2}
          />
            <EventTracker onClick={this.click}  onPointerMove={this.changeHover}/>
          {/* <SelectionState selection={selectionWeek} /> */}
        </Chart>
      );
    }

    const last12monthsTable = () => {
      return (
        <Chart
          data={dataMonths}
          height={159}
        >
          <ValueScale factory={scale} modifyDomain={modifyDomain} />
          <ArgumentAxis labelComponent={NewlineText} />
          <ValueAxis max={24} />
          <BarSeries
            name="last12monthsTable"
            valueField="no"
            argumentField="month"
            barWidth={0.5}
            color={'#0000EE'}
            pointComponent={newBar3}
          />
            <EventTracker onClick={this.click}  onPointerMove={this.changeHover}/>
          {/* <SelectionState selection={selectionMonths} /> */}
        </Chart>
      );
    }

    return (
      <ThemeProvider theme={theme}>
        {load()}
        <div>
          <Grid container >
            <Grid item xs={6}>
              <Typography variant='h4' style={{ marginBottom: '16px' }}>Volumn distrubution</Typography>
              <Typography variant='h5' style={{ marginBottom: '30px' }}>Single day (24 hours)</Typography>
            </Grid>
            <Grid item xs={6} align='right' style={selection24.length ? {} : { marginTop: '16px' }}>
              {/* eslint-disable-next-line */}
              <Typography variant='h6'>Total &nbsp;&nbsp;<a className={classes.count}>{dsum}</a></Typography>
              {/* eslint-disable-next-line */}
              <Typography variant='subtitle2'>{selection24.length ? <Typography variant='h6'>no. of visit&nbsp;&nbsp;<a className={classes.count}>{data24[selection24[0].point]['no']}</a></Typography> : "Click the bar to see the no. of visit each hour"}</Typography>
            </Grid>
          </Grid>
          {last24hoursTable()}
          <Grid container style={{ marginTop: '15px' }}>
            <Grid item xs={6} style={{ marginBottom: '18px' }}>
              <Typography variant='h4' >Period</Typography>
              <Typography variant='subtitle2' style={{ marginBottom: '12px' }}>from your selected date or period</Typography>
              <Button variant="contained"
                style={{ marginRight: '16px' }}
                className={classes.button}
                color={this.state.period ? "secondary" : "primary"}
                onClick={(event) => { handleTable(event) }}
              >
                <Typography variant='subtitle2' style={this.state.period ? {} : { color: '#828282' }}>12 months</Typography>
              </Button>
              <Button variant='contained'
                className={classes.button}
                color={this.state.period ? "primary" : "secondary"}
                onClick={(event) => { handleTable(event) }}
              >
                <Typography variant='subtitle2' style={this.state.period ? { color: '#828282' } : {}}>1 Week</Typography>
              </Button>
            </Grid>
            <Grid item xs={6} align='right' style={selectionMonths.length || selectionWeek.length ? {} : { marginTop: '16px' }}>
              {/* eslint-disable-next-line */}
              <Typography variant='h6'>Total &nbsp;&nbsp;<a className={classes.count}>{this.state.period ? msum : wsum}</a></Typography>
              {/* eslint-disable-next-line */}
              {this.state.period ? <Typography variant='subtitle2'>{selectionMonths.length ? <Typography variant='h6'>no. of visit&nbsp;&nbsp;<a className={classes.count}>{dataMonths[selectionMonths[0].point]['no']}</a></Typography> : "Click the bar to see the no. of visit each month"}</Typography> :
                //eslint-disable-next-line
                <Typography variant='subtitle2'>{selectionWeek.length ? <Typography variant='h6'>no. of visit&nbsp;&nbsp;<a className={classes.count}>{dataWeek[selectionWeek[0].point]['no']}</a></Typography> : "Click the bar to see the no. of visit each day"}</Typography>}
            </Grid>
          </Grid>
          {this.state.period ? last12monthsTable() : weekTable()}
        </div>
      </ThemeProvider>);
  }
}

export default withStyles(style)(App);